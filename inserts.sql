/*create table Course_Taken(id integer not null identity(1,1),
						  sid integer not null,
						  course_id integer not null,
						  section_id integer not null,
						  quarter varchar(20) not null,
						  year_taken integer not null,
						  units_taken_for integer not null,
						  grade varchar(20) not null,
						  primary key (id),
						  unique(sid, course_id, quarter, year_taken),
						  foreign key (sid) references Student(sid),
						  foreign key (course_id) references Course(course_id),
						  foreign key (section_id) references Class_(section_id));*/
					 
/*insert into Course_Taken(sid, course_id, section_id, quarter, year_taken, units_taken_for) values (,,,'',,);*/

/*
1a
Course
Class
Course_Class
Course_Taken STUDENT_ENROLLMENT
Enrolled STUDENT_SECTION
*/

/*
1a)
SELECT *
FROM Student
WHERE enrolled=1;

SELECT ct3.quarter, ct3.year_taken, ct3.section_id, ct3.course_id, ct3.units_taken_for, ct3.grade_option, ct3.course_name, ct3.name
FROM course_taken3 ct3, Student s
WHERE s.sid = ct3.sid AND ct3.quarter = 'Winter' AND ct3.year_taken = 2016;

1b)
SELECT DISTINCT course_name, quarter, year_taken
FROM course_taken3;


SELECT s.sid, s.firstname, s.middlename, s.lastname, s.ssn, s.residency, ct3.grade_option, ct3.units_taken_for
FROM Student s, course_taken3 ct3
WHERE s.sid = ct3.sid AND ct3.course_name = 'CSE 8A' AND ct3.quarter = 'Fall' and ct3.year_taken = 2014;

1c)
SELECT * FROM Student;

CREATE VIEW student_quarter_grades AS
SELECT s.sid, c.section_id, c.enrollment_limit, c.quarter, c.year_, c.title, ct.units_taken_for, ct.grade, gc.number_grade
FROM Class_ c, Course_Taken ct, Student s, grade_conversion gc
WHERE s.sid = ct.sid AND ct.section_id = c.section_id AND ct.grade = gc.letter_grade;

SELECT section_id, enrollment_limit, quarter, year_, title, units_taken_for, grade
FROM student_quarter_grades, Student s
WHERE s.ssn = 123456789 AND s.sid = student_quarter_grades.sid
ORDER BY quarter, year_;

SELECT quarter, year_, avg(number_grade) AS quarter_gpa
FROM student_quarter_grades, Student s
WHERE s.ssn = 123456789 AND s.sid = student_quarter_grades.sid
GROUP BY quarter, year_
ORDER BY quarter, year_;

SELECT avg(number_grade) AS cumulative_gpa
FROM student_quarter_grades, Student s
WHERE s.ssn = 123456789 AND s.sid = student_quarter_grades.sid;

1d)
SELECT * FROM Student WHERE enrolled = 1;

SELECT deg.degree_id, dept.department_name, deg.degree_type
FROM Department dept, Degree deg
WHERE dept.department_id = deg.department_id AND deg.degree_type ='B.Sc';

CREATE VIEW course_taken2 AS
SELECT ct.*, cc.course_id, c.category, co.department_id
FROM Course_Taken ct, Course_Class cc, Course c, Course_Offered co
WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id
ORDER BY sid;

CREATE VIEW all_deg_info AS
SELECT deg.*, dept.department_name, dr.category, dr.units_completed
FROM Degree deg, Department dept, Degree_Required dr
WHERE deg.degree_id = dr.degree_id AND deg.department_id = dept.department_id;

SELECT adi.department_name, adi.degree_type, adi.category, adi.units_completed AS units_req, sum(c.units_taken_for) AS units_done, (adi.units_completed - sum(c.units_taken_for)) AS units_left
FROM course_taken2 c, Student s, all_deg_info adi
WHERE s.ssn = 123456789 AND s.sid = c.sid AND c.category = adi.category and c.department_id = adi.department_id AND adi.degree_type = 'B.Sc' and adi.degree_id = 1 AND ((c.grade IS NULL) OR c.grade IS NOT NULL AND c.grade <> 'F')
GROUP BY adi.department_name, adi.degree_type, adi.category, adi.units_completed;

2
SELECT dept.department_name, deg.degree_type, dr.category, dr.units_completed AS units_req, sum(ct3.units_taken_for) AS units_done, (dr.units_completed - sum(ct3.units_taken_for)) AS units_left
FROM Department dept, Degree deg, Degree_required dr, Student s, course_taken3 ct3, Course_Offered co
WHERE deg.department_id = dept.department_id AND dept.department_name = 'Computer Science' AND degree_type = 'B.Sc' AND dr.degree_id = deg.degree_id AND s.ssn = 1 and s.sid = ct3.sid and ct3.category = dr.category and co.course_id = ct3.course_id and co.department_id = dept.department_id
GROUP BY dept.department_name, deg.degree_type, dr.category, dr.units_completed;

SELECT dept.department_name, deg.degree_type, ec.category, dr.units_completed AS units_req, sum(ct3.units_taken_for) AS units_done, (dr.units_completed - sum(ct3.units_taken_for)) AS units_left
FROM Department dept, Degree deg, Degree_required dr, Student s, course_taken3 ct3, extra_category ec, Course_Offered co
WHERE deg.department_id = dept.department_id AND dept.department_name = 'Computer Science' AND degree_type = 'B.Sc' AND dr.degree_id = deg.degree_id AND s.ssn = 1 and s.sid = ct3.sid and ct3.category = dr.category AND ct3.course_id = ec.course_id and co.course_id = ec.course_id and co.department_id = dept.department_id
GROUP BY dept.department_name, deg.degree_type, ec.category, dr.units_completed;

1e2)
SELECT c.concentration_name, units AS units_req, sum(units_taken_for) AS units_done, c.min_gpa, avg(gc.number_grade) AS gpa
FROM course_taken3 ct3, Student s, concentration c, grade_conversion gc
WHERE s.ssn = 16 AND s.sid = ct3.sid and ct3.course_id = c.course_id and ct3.grade = gc.letter_grade
GROUP BY c.concentration_name, c.units, min_gpa;

1e3)
SELECT t2.course_name, cl.quarter, cl.year_
FROM
(SELECT * 
FROM
(SELECT course_name
FROM concentration c, course co
WHERE c.course_id = co.course_id) t1
EXCEPT
(SELECT ct3.course_name 
FROM student s, concentration c, course_taken3 ct3
WHERE s.ssn = 19 and s.sid = ct3.sid and c.course_id = ct3.course_id)) t2, Course co, Class_ cl, Course_Class cc
WHERE t2.course_name = co.course_name and cc.section_id = cl.section_id and cc.course_id = co.course_id AND cl.year_ >= 2016 AND NOT (cl.year_ = 2016 and cl.quarter = 'Winter');


1e)
SELECT dept.department_name, deg.degree_type, dr.category, dr.units_completed AS units_req, sum(ct3.units_taken_for) AS units_done, (dr.units_completed - sum(ct3.units_taken_for)) AS units_left
FROM Department dept, Degree deg, Degree_required dr, Student s, course_taken3 ct3, Course_Offered co
WHERE deg.department_id = dept.department_id AND dept.department_name = 'Computer Science' AND degree_type = 'M.S' AND dr.degree_id = deg.degree_id AND s.ssn = 17 and s.sid = ct3.sid and ct3.category = dr.category and co.course_id = ct3.course_id and co.department_id = dept.department_id AND deg.concentration is null
GROUP BY dept.department_name, deg.degree_type, dr.category, dr.units_completed;

SELECT dept.department_name, deg.degree_type, ec.category, dr.units_completed AS units_req, sum(ct3.units_taken_for) AS units_done, (dr.units_completed - sum(ct3.units_taken_for)) AS units_left
FROM Department dept, Degree deg, Degree_required dr, Student s, course_taken3 ct3, extra_category ec, Course_Offered co
WHERE deg.department_id = dept.department_id AND dept.department_name = 'Computer Science' AND degree_type = 'M.Sc' AND dr.degree_id = deg.degree_id AND s.ssn = 17 and s.sid = ct3.sid and ct3.category = dr.category AND ct3.course_id = ec.course_id and co.course_id = ec.course_id and co.department_id = dept.department_id and deg.concentration is null
GROUP BY dept.department_name, deg.degree_type, ec.category, dr.units_completed;

SELECT s.* from Student s, MS m WHERE s.sid = m.sid and s.enrolled = 1;

SELECT deg.degree_id, dept.department_name, deg.degree_type FROM Department dept, Degree deg WHERE dept.department_id = deg.department_id AND deg.degree_type ='M.S' and deg.concentration IS NULL;
i)
SELECT adi.department_name, adi.degree_type, deg.concentration, adi.category, adi.units_completed AS units_req, sum(c.units_taken_for) AS units_done, (adi.units_completed - sum(c.units_taken_for)) AS units_left
FROM course_taken2 c, Student s, all_deg_info adi, MS m, Degree deg
WHERE s.ssn = 010101012 AND s.sid = m.sid AND m.sid = c.sid AND deg.degree_id = adi.degree_id AND c.category = adi.category and c.department_id = adi.department_id AND adi.degree_type = 'M.S' and adi.degree_id = 8 AND ((c.grade IS NULL) OR c.grade IS NOT NULL AND c.grade <> 'F')
GROUP BY adi.department_name, adi.degree_type, adi.category, adi.units_completed, deg.concentration;
ii) need to handle in Java
SELECT adi.department_name, adi.degree_type, adi.category, adi.units_completed AS units_req, sum(c.units_taken_for) AS units_done, (adi.units_completed - sum(c.units_taken_for)) AS units_left, adi.min_gpa
FROM course_taken2 c, Student s, all_deg_info adi, MS m
WHERE s.ssn = 010101012 AND s.sid = m.sid AND m.sid = c.sid AND c.department_id = adi.department_id AND adi.degree_type = 'M.S' AND ((c.grade IS NULL) OR c.grade IS NOT NULL AND c.grade <> 'F') AND c.category = adi.category
GROUP BY adi.department_name, adi.degree_type, adi.category, adi.units_completed, adi.min_gpa;

SELECT adi.department_name, adi.degree_type, adi.category, adi.units_completed AS units_req, sum(c.units_taken_for) AS units_done, (adi.units_completed - sum(c.units_taken_for)) AS units_left, adi.min_gpa
FROM course_taken2 c, Student s, all_deg_info adi, MS m
WHERE s.ssn = 010101012 AND s.sid = m.sid AND m.sid = c.sid AND c.category = adi.category and c.department_id = adi.department_id AND adi.degree_type = 'M.S' AND ((c.grade IS NULL) OR c.grade IS NOT NULL AND c.grade <> 'F')
GROUP BY adi.department_name, adi.degree_type, adi.category, adi.units_completed, adi.min_gpa;

iii)
CREATE VIEW ms_gpa as
SELECT s.sid, c.section_id, c.quarter, c.year_, c.title, ct.units_taken_for, ct.grade, gc.number_grade, co.category
FROM Class_ c, Course_Taken ct, Student s, grade_conversion gc, Course co, Course_class cc
WHERE s.sid = ct.sid AND ct.section_id = c.section_id AND ct.grade = gc.letter_grade AND cc.section_id = ct.section_id AND cc.course_id = co.course_id;

SELECT adi.department_name, adi.degree_type, adi.category, adi.concentration, adi.units_completed AS units_req, sum(c.units_taken_for) AS units_done, (adi.units_completed - sum(c.units_taken_for)) AS units_left, adi.min_gpa, avg(msg.number_grade) AS gpa
FROM course_taken2 c, Student s, all_deg_info adi, MS m, ms_gpa msg
WHERE s.ssn = 010101012 AND s.sid = m.sid AND m.sid = c.sid AND c.category = adi.category and c.department_id = adi.department_id AND adi.degree_type = 'M.S' AND ((c.grade IS NULL) OR c.grade IS NOT NULL AND c.grade <> 'F')
AND msg.category = c.category AND msg.sid = c.sid AND msg.section_id = c.section_id AND degree_id = 8
GROUP BY adi.department_name, adi.degree_type, adi.category, adi.units_completed, adi.min_gpa, adi.concentration;


3)
ii)
SELECT ct.*, cc.course_id, c.course_name, c.category, co.department_id, tb.name
FROM Course_Taken ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_
ORDER BY course_name;

SELECT DISTINCT course_id, course_name, name, quarter, year_taken FROM course_taken3 WHERE year_taken <> 2016;

SELECT 'A' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND quarter = 'Fall' AND year_taken = 2014 AND name = 'Justin Beiber' AND grade LIKE 'A%';

SELECT 'B' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND quarter = 'Fall' AND year_taken = 2014 AND name = 'Justin Beiber' AND grade LIKE 'B%';

SELECT 'C' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND quarter = 'Fall' AND year_taken = 2014 AND name = 'Justin Beiber' AND grade LIKE 'C%';

SELECT 'D' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND quarter = 'Fall' AND year_taken = 2014 AND name = 'Justin Beiber' AND grade LIKE 'D%';

SELECT 'Other' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 221' AND quarter = 'Winter' AND year_taken = 2016 AND name = 'Kelly Clarkson' AND grade NOT LIKE 'A%' AND grade NOT LIKE 'B%' AND grade NOT LIKE 'C%' AND grade NOT LIKE 'D%' ;

iii)
SELECT ct.*, cc.course_id, c.course_name, c.category, co.department_id, tb.name
FROM Course_Taken ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_
ORDER BY course_name;

SELECT DISTINCT course_id, course_name, name FROM course_taken3 WHERE year_taken <> 2016;

SELECT 'A' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND name = 'Justin Beiber' AND grade LIKE 'A%';

SELECT 'B' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND name = 'Justin Beiber' AND grade LIKE 'B%';

SELECT 'C' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND name = 'Justin Beiber' AND grade LIKE 'C%';

SELECT 'D' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND name = 'Justin Beiber' AND grade LIKE 'D%';

SELECT 'Other' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 221' AND name = 'Kelly Clarkson' AND grade NOT LIKE 'A%' AND grade NOT LIKE 'B%' AND grade NOT LIKE 'C%' AND grade NOT LIKE 'D%' ;

iv)
SELECT ct.*, cc.course_id, c.course_name, c.category, co.department_id, tb.name
FROM Course_Taken ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_
ORDER BY course_name;

SELECT DISTINCT course_id, course_name FROM course_taken3 WHERE year_taken <> 2016;

SELECT 'A' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND grade LIKE 'A%';

SELECT 'B' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND grade LIKE 'B%';

SELECT 'C' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND grade LIKE 'C%';

SELECT 'D' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 8A' AND grade LIKE 'D%';

SELECT 'Other' as grade, count(*) as number
FROM course_taken3
WHERE course_name = 'CSE 221' AND grade NOT LIKE 'A%' AND grade NOT LIKE 'B%' AND grade NOT LIKE 'C%' AND grade NOT LIKE 'D%' ;

v)
SELECT ct.*, cc.course_id, c.course_name, c.category, co.department_id, tb.name
FROM Course_Taken ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_
ORDER BY course_name;

SELECT DISTINCT course_id, course_name, name FROM course_taken3 WHERE year_taken <> 2016;

SELECT avg(gc.number_grade) AS avg_gpa
FROM grade_conversion gc, course_taken3 ct3
WHERE ct3.grade = gc.letter_grade AND ct3.course_name = 'CSE 250A' AND name = 'Bjork';

5i) 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER cpqg2InsertTrigger 
   ON  course_taken
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @quarter varchar(255);
	DECLARE @year_taken integer;
	DECLARE @grade varchar(255);
	DECLARE @professor varchar(255);
	DECLARE @course_name varchar(255);

	SET @quarter = (SELECT quarter FROM INSERTED);
	SET @year_taken = (SELECT year_taken FROM INSERTED);
	SET @grade = (SELECT grade FROM INSERTED);
	SET @course_name = (SELECT c.course_name
						FROM INSERTED ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
						WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_);
	SET @professor = (SELECT tb.name
					  FROM INSERTED ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
					  WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_);

	if (EXISTS (SELECT course_name, professor, quarter, year_ FROM cpqg2 WHERE course_name = @course_name AND professor = @professor AND quarter = @quarter AND year_ = @year_taken))
		BEGIN
			if(@grade LIKE 'A%')
				BEGIN
					UPDATE cpqg2
					SET a_grade = a_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@grade LIKE 'B%')
				BEGIN
					UPDATE cpqg2
					SET b_grade = b_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@grade LIKE 'C%')
				BEGIN
					UPDATE cpqg2
					SET c_grade = c_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@grade LIKE 'D%')
				BEGIN
					UPDATE cpqg2
					SET d_grade = d_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else
				BEGIN
					UPDATE cpqg2
					SET other_grade = other_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
		END

	else
		BEGIN
			if(@grade LIKE 'A%')
				BEGIN
					INSERT INTO cpqg2(course_name, quarter, year_, professor, a_grade, b_grade, c_grade, d_grade, other_grade) values (@course_name, @quarter, @year_taken, @professor, 1, 0, 0 ,0 ,0);
				END
			else if(@grade LIKE 'B%')
				BEGIN
					INSERT INTO cpqg2(course_name, quarter, year_, professor, a_grade, b_grade, c_grade, d_grade, other_grade) values (@course_name, @quarter, @year_taken, @professor, 0, 1, 0 ,0 ,0);
				END
			else if(@grade LIKE 'C%')
				BEGIN
					INSERT INTO cpqg2(course_name, quarter, year_, professor, a_grade, b_grade, c_grade, d_grade, other_grade) values (@course_name, @quarter, @year_taken, @professor, 0, 0, 1 ,0 ,0);
				END
			else if(@grade LIKE 'D%')
				BEGIN
					INSERT INTO cpqg2(course_name, quarter, year_, professor, a_grade, b_grade, c_grade, d_grade, other_grade) values (@course_name, @quarter, @year_taken, @professor, 0, 0, 0 ,1 ,0);
				END
			else
				BEGIN
					INSERT INTO cpqg2(course_name, quarter, year_, professor, a_grade, b_grade, c_grade, d_grade, other_grade) values (@course_name, @quarter, @year_taken, @professor, 0, 0, 0 ,0 ,1);
				END
		END
	

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER cpqg2UpdateTrigger 
   ON  course_taken
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @quarter varchar(255);
	DECLARE @year_taken integer;
	DECLARE @oldGrade varchar(255);
	DECLARE @newGrade varchar(255);
	DECLARE @professor varchar(255);
	DECLARE @course_name varchar(255);

	SET @quarter = (SELECT quarter FROM INSERTED);
	SET @year_taken = (SELECT year_taken FROM INSERTED);
	SET @oldGrade = (SELECT grade FROM DELETED);
	SET @newGrade = (SELECT grade FROM INSERTED);
	SET @course_name = (SELECT c.course_name
						FROM INSERTED ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
						WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_);
	SET @professor = (SELECT tb.name
					  FROM INSERTED ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
					  WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_);

		BEGIN
			if(@oldGrade LIKE 'A%')
				BEGIN
					UPDATE cpqg2
					SET a_grade = a_grade - 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@oldGrade LIKE 'B%')
				BEGIN
					UPDATE cpqg2
					SET b_grade = b_grade - 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@oldGrade LIKE 'C%')
				BEGIN
					UPDATE cpqg2
					SET c_grade = c_grade - 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@oldGrade LIKE 'D%')
				BEGIN
					UPDATE cpqg2
					SET d_grade = d_grade - 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else
				BEGIN
					UPDATE cpqg2
					SET other_grade = other_grade - 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
		END

		BEGIN
			if(@newGrade LIKE 'A%')
				BEGIN
					UPDATE cpqg2
					SET a_grade = a_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@newGrade LIKE 'B%')
				BEGIN
					UPDATE cpqg2
					SET b_grade = b_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@newGrade LIKE 'C%')
				BEGIN
					UPDATE cpqg2
					SET c_grade = c_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else if(@newGrade LIKE 'D%')
				BEGIN
					UPDATE cpqg2
					SET d_grade = d_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
			else
				BEGIN
					UPDATE cpqg2
					SET other_grade = other_grade + 1
					WHERE course_name = @course_name AND quarter = @quarter AND year_ = @year_taken AND professor = @professor;
				END
		END
END
GO


SELECT c.course_name, ct.quarter, ct.year_taken, tb.name, ct.grade
FROM Course_Taken ct, Course_Class cc, Course c, Course_Offered co, Taught_By tb
WHERE ct.section_id = cc.section_id AND cc.course_id = c.course_id AND c.course_id = co.course_id AND c.course_id  = tb.course_id AND ct.quarter = tb.quarter and ct.year_taken = tb.year_;
*/

insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Ren', 'Guang', 'Xi', 123456789, 'California', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Liang', 'Mu', 'Cheng', 987654321, 'Foreign', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Xue', 'Shan', 'Shan', 321654987, 'Foreign', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Feng', NULL, 'Teng', 594869483, 'California', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Shen', 'Ya', 'Yin', 192847563, 'California', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Qin', 'Yu', 'Jiang', 192938271, 'California', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Cheong', 'Song', 'Yi', 493800192, 'Foreign', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Do', 'Min', 'Joon', 019201928, 'Foreign', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Kim', 'Tae', 'Yeon', 991029391, 'Foreign', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Hwang', 'Mi', 'Young', 910293817, 'California', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Jung', 'Soo', 'Yeon', 910392019, 'California', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Lee', 'Soon', 'Kyu', 918373747, 'Foreign', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Kim', 'Hyo', 'Yeon', 916253416, 'Foreign', 0);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Ju', 'Seo', 'Hyun', 918374610, 'Foreign', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Kwon', 'Yu', 'Ri', 999102910, 'Foreign', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Choi', 'Soo', 'Young', 991817171, 'Foreign', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Im', 'Yoon', 'Ah', 990088171, 'Foreign', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Ko', 'Ching', 'Teng', 127640183, 'Non-CA US', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Shen', 'Chia', 'Yi', 738193846, 'Non-CA US', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Wang', 'Li', 'Hong', 103847164, 'Non-CA US', 1);
insert into Student (firstname, middlename, lastname, ssn, residency, enrolled) values ('Zhao', 'Zi', 'Long', 10101012, 'Foreign', 1);

insert into Graduate (sid) values (21);

insert into MS (sid, concentration) values (21, NULL);

insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (1, 4, 'Letter', 0, 'Lower Division', 'CSE 1', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Letter', 0, 'Lower Division', 'CSE 99', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 5, 'Any', 0, 'Lower Division', 'CHIN 20AN', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (5, 5, 'Any', 1, 'Upper Division', 'CHIN 100AN', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (1, 1, 'S/U', 0, 'Lower Division', 'SIO 1', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (8, 8, 'Letter', 1, 'Upper Division', 'CSE 197', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (1, 4, 'Any', 0, 'Other', 'CSE 599', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (2, 4, 'Any', 1, 'Lower Division', 'MUS 1', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (1, 1, 'S/U', 0, 'Other', 'MUS 99', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Any', 0, 'Upper Division', 'CSE 190', 1);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Any', 0, 'Upper Division', 'CSE 100', 0);
/*test part 3*/
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (1, 4, 'Any', 0, 'Upper Division', 'CSE 2', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (2, 4, 'Any', 0, 'Upper Division', 'CSE 3', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (3, 4, 'Any', 0, 'Upper Division', 'CSE 4', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Any', 0, 'Upper Division', 'CSE 5', 0);
/*test part 5*/
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (1, 4, 'Any', 0, 'Graduate', 'BIO 201', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (2, 4, 'Any', 0, 'Graduate', 'BIO 210', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (3, 4, 'Any', 0, 'Graduate', 'BIO 232', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Any', 0, 'Graduate', 'BIO 250', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Any', 0, 'Animal Biology', 'BIO 251', 0);
insert into Course (min_units, max_units, grade_options, lab_work_req, category, course_name, consent_req) values (4, 4, 'Any', 0, 'Animal Biology', 'BIO 252', 0);

insert into Class_ (enrollment_limit, quarter, year_, title) values (250,'Spring',2009,'Introduction to Computers');
insert into Class_ (enrollment_limit, quarter, year_, title) values (200,'Spring',2009,'Introduction to Computers');
insert into Class_ (enrollment_limit, quarter, year_, title) values (120,'Spring',2009,'Intro to C++');
insert into Class_ (enrollment_limit, quarter, year_, title) values (120,'Spring',2009,'Intro to C++');
insert into Class_ (enrollment_limit, quarter, year_, title) values (40, 'Spring',2009,'Intermediate Mandarin Chinese');
insert into Class_ (enrollment_limit, quarter, year_, title) values (40, 'Spring',2009,'Intermediate Mandarin Chinese');
insert into Class_ (enrollment_limit, quarter, year_, title) values (40, 'Spring',2009,'Advanced Mandarin Chinese');
insert into Class_ (enrollment_limit, quarter, year_, title) values (40, 'Spring',2009,'Advanced Mandarin Chinese');
insert into Class_ (enrollment_limit, quarter, year_, title) values (90, 'Spring',2009,'Oceanography');
insert into Class_ (enrollment_limit, quarter, year_, title) values (70, 'Spring',2009,'Oceanography');
insert into Class_ (enrollment_limit, quarter, year_, title) values (500,'Spring',2009,'Independent Study - Computer Science');
insert into Class_ (enrollment_limit, quarter, year_, title) values (500,'Spring',2009,'Independent Study - Computer Science');
insert into Class_ (enrollment_limit, quarter, year_, title) values (80, 'Spring',2009,'Tutoring - Computer Science');
insert into Class_ (enrollment_limit, quarter, year_, title) values (80, 'Spring',2009,'Tutoring - Computer Science');
insert into Class_ (enrollment_limit, quarter, year_, title) values (250,'Spring',2009,'Introduction to Modern Music');
insert into Class_ (enrollment_limit, quarter, year_, title) values (125,'Spring',2009,'Introduction to Modern Music');
insert into Class_ (enrollment_limit, quarter, year_, title) values (75, 'Spring',2009,'Contemporary Music');
insert into Class_ (enrollment_limit, quarter, year_, title) values (45, 'Spring',2009,'Contemporary Music');
insert into Class_ (enrollment_limit, quarter, year_, title) values (45, 'Spring',2009,'Data Mining');
insert into Class_ (enrollment_limit, quarter, year_, title) values (60, 'Spring',2009,'Data Mining');
insert into Class_ (enrollment_limit, quarter, year_, title) values (200,'Spring',2009,'Advanced Data Structures');
insert into Class_ (enrollment_limit, quarter, year_, title) values (180,'Spring',2009,'Advanced Data Structures');
/*test part 3*/
insert into Class_ (enrollment_limit, quarter, year_, title) values (45, 'Spring',2008,'Databases 1');
insert into Class_ (enrollment_limit, quarter, year_, title) values (60, 'Spring',2008,'Databases 2');
insert into Class_ (enrollment_limit, quarter, year_, title) values (40, 'Spring',2008,'Databases 3');
insert into Class_ (enrollment_limit, quarter, year_, title) values (100, 'Spring',2008,'Databases 4');
/*test part 5*/
insert into Class_ (enrollment_limit, quarter, year_, title) values (100,'Spring',2008,'Molecular Biology');
insert into Class_ (enrollment_limit, quarter, year_, title) values (110,'Spring',2008,'Cell Biology');
insert into Class_ (enrollment_limit, quarter, year_, title) values (120,'Spring',2008,'Bioengineering');
insert into Class_ (enrollment_limit, quarter, year_, title) values (130,'Spring',2008,'Bioengineering');
insert into Class_ (enrollment_limit, quarter, year_, title) values (140,'Spring',2008,'Evolutionary Biology');
insert into Class_ (enrollment_limit, quarter, year_, title) values (140,'Spring',2008,'Animal Biology');
insert into Class_ (enrollment_limit, quarter, year_, title) values (140,'Spring',2008,'Animal Biology 2');

insert into Course_Class (course_id, section_id) values (1,1);
insert into Course_Class (course_id, section_id) values (1,2);
insert into Course_Class (course_id, section_id) values (2,3);
insert into Course_Class (course_id, section_id) values (2,4);
insert into Course_Class (course_id, section_id) values (3,5);
insert into Course_Class (course_id, section_id) values (3,6);
insert into Course_Class (course_id, section_id) values (4,7);
insert into Course_Class (course_id, section_id) values (4,8);
insert into Course_Class (course_id, section_id) values (5,9);
insert into Course_Class (course_id, section_id) values (5,10);
insert into Course_Class (course_id, section_id) values (6,11);
insert into Course_Class (course_id, section_id) values (6,12);
insert into Course_Class (course_id, section_id) values (7,13);
insert into Course_Class (course_id, section_id) values (7,14);
insert into Course_Class (course_id, section_id) values (8,15);
insert into Course_Class (course_id, section_id) values (8,16);
insert into Course_Class (course_id, section_id) values (9,17);
insert into Course_Class (course_id, section_id) values (9,18);
insert into Course_Class (course_id, section_id) values (10,19);
insert into Course_Class (course_id, section_id) values (10,20);
insert into Course_Class (course_id, section_id) values (11,21);
insert into Course_Class (course_id, section_id) values (11,22);
/*test part 3*/
insert into Course_Class (course_id, section_id) values (12,23);
insert into Course_Class (course_id, section_id) values (13,24);
insert into Course_Class (course_id, section_id) values (14,25);
insert into Course_Class (course_id, section_id) values (15,26);
/*test part 5*/
insert into Course_Class (course_id, section_id) values (16,27);
insert into Course_Class (course_id, section_id) values (17,28);
insert into Course_Class (course_id, section_id) values (18,29);
insert into Course_Class (course_id, section_id) values (18,30);
insert into Course_Class (course_id, section_id) values (19,31);
insert into Course_Class (course_id, section_id) values (20,32);
insert into Course_Class (course_id, section_id) values (21,33);




insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (1,1,'Letter',4);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (1,3,'Letter',4);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (1,5,'Letter',4);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (1,7,'S/U',5);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (8,9,'S/U',1);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (8,11,'Letter',8);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (8,13,'Letter',4);
insert into Enrolled (sid, section_id, grade_options, units_taken_for) values (8,15,'Letter',2);

insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (1,1,'SPRING',2009,4,'Letter');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (1,3,'SPRING',2009,4,'Letter');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (1,5,'SPRING',2009,4,'Letter');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (1,7,'SPRING',2009,5,'S/U');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (8,9,'SPRING',2009,1,'S/U');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (8,11,'SPRING',2009,8,'Letter');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (8,13,'SPRING',2009,4,'Letter');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (8,15,'SPRING',2009,2,'Letter');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option) values (4,1,'SPRING',2009,4,'Letter');
/*test part 3*/
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (1,23,'SPRING',2008,1,'S/U', 'S');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (1,24,'SPRING',2008,2,'Letter', 'A+');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (1,25,'SPRING',2008,3,'Letter', 'A+');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (1,26,'SPRING',2008,4,'Letter', 'A');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (7,26,'SPRING',2008,4,'Letter', 'A+');
/*tets part 5*/
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (21,27,'SPRING',2008,4,'Letter', 'A+');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (21,28,'SPRING',2008,4,'Letter', 'A');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (21,29,'SPRING',2008,4,'Letter', 'B+');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (21,31,'SPRING',2008,4,'Letter', 'A-');
insert into Course_Taken(sid, section_id, quarter, year_taken, units_taken_for, grade_option, grade) values (21,32,'SPRING',2008,4,'Letter', 'A-');

insert into grade_conversion values('A+', 4.3);
insert into grade_conversion values('A', 4);
insert into grade_conversion values('A-', 3.7);
insert into grade_conversion values('B+', 3.4);
insert into grade_conversion values('B', 3.1);
insert into grade_conversion values('B-', 2.8);
insert into grade_conversion values('C+', 2.5);
insert into grade_conversion values('C', 2.2);
insert into grade_conversion values('C-', 1.9);
insert into grade_conversion values('D', 1.6);
insert into grade_conversion values('F', 0.0);
insert into grade_conversion values('S', NULL);
insert into grade_conversion values('U', NULL);

insert into Department(department_name) values ('Computer Science');
insert into Department(department_name) values ('Electrical Engineering');
insert into Department(department_name) values ('Chinese');
insert into Department(department_name) values ('Music');
insert into Department(department_name) values ('Physics');
insert into Department(department_name) values ('Oceanography');
insert into Department(department_name) values ('Biology');

insert into Degree(department_id, degree_type, concentration) values (1,'B.Sc',NULL);
insert into Degree(department_id, degree_type, concentration) values (1,'B.A',NULL);
insert into Degree(department_id, degree_type, concentration) values (2,'B.Sc',NULL);
insert into Degree(department_id, degree_type, concentration) values (3,'B.A',NULL);
insert into Degree(department_id, degree_type, concentration) values (4,'B.A',NULL);
insert into Degree(department_id, degree_type, concentration) values (5,'B.Sc',NULL);
insert into Degree(department_id, degree_type, concentration) values (6,'B.Sc',NULL);
insert into Degree(department_id, degree_type, concentration) values (7,'M.S', 'Applied Biology');
insert into Degree(department_id, degree_type, concentration) values (7,'M.S', 'Animal Biology');


insert into Degree_Required(degree_id, category, units_completed, min_gpa) values (1,'Lower Division', 50, NULL);
insert into Degree_Required(degree_id, category, units_completed, min_gpa) values (1,'Upper Division',100, NULL);
insert into Degree_Required(degree_id, category, units_completed, min_gpa) values (8,'Graduate',16, 2.0);
insert into Degree_Required(degree_id, category, units_completed, min_gpa) values (9,'Animal Biology',100, 2.0);

insert into Course_Offered(course_id, department_id) values (1,1);
insert into Course_Offered(course_id, department_id) values (2,1);
insert into Course_Offered(course_id, department_id) values (3,3);
insert into Course_Offered(course_id, department_id) values (4,3);
insert into Course_Offered(course_id, department_id) values (5,6);
insert into Course_Offered(course_id, department_id) values (6,1);
insert into Course_Offered(course_id, department_id) values (7,1);
insert into Course_Offered(course_id, department_id) values (8,4);
insert into Course_Offered(course_id, department_id) values (9,4);
insert into Course_Offered(course_id, department_id) values (10,1);
insert into Course_Offered(course_id, department_id) values (11,1);
insert into Course_Offered(course_id, department_id) values (12,1);
insert into Course_Offered(course_id, department_id) values (13,1);
insert into Course_Offered(course_id, department_id) values (14,1);
insert into Course_Offered(course_id, department_id) values (15,1);
insert into Course_Offered(course_id, department_id) values (16,7);
insert into Course_Offered(course_id, department_id) values (17,7);
insert into Course_Offered(course_id, department_id) values (18,7);
insert into Course_Offered(course_id, department_id) values (19,7);
insert into Course_Offered(course_id, department_id) values (20,7);
insert into Course_Offered(course_id, department_id) values (21,7);


